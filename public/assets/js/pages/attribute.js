$(document).ready(function() {

    $('#attribute-table').dataTable({
        "language": {
            search: "_INPUT_",
            searchPlaceholder: "Search..."
        }
    });

    $("#attribute-table_filter input[type=search]").attr("placeholder", "search...");

} );