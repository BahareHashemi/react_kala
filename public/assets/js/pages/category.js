$(document).ready(function() {

    $('#category-table').dataTable({
        "language": {
            search: "_INPUT_",
            searchPlaceholder: "Search..."
        }
    });

    $("#category-table_filter input[type=search]").attr("placeholder", "search...");

} );