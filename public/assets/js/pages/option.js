$(document).ready(function() {

    $('#option-table').dataTable({
        "language": {
            search: "_INPUT_",
            searchPlaceholder: "Search..."
        }
    });

    $("#option-table_filter input[type=search]").attr("placeholder", "search...");

} );