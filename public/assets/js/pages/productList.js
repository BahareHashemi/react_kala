$(document).ready(function() {

    $('#product-table').dataTable({
        "language": {
            search: "_INPUT_",
            searchPlaceholder: "Search..."
        }
    });

    $("#product-table_filter input[type=search]").attr("placeholder", "search...");

} );