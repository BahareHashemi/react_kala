$(document).ready(function() {

    $('#brand-table').dataTable({
        "language": {
            search: "_INPUT_",
            searchPlaceholder: "Search..."
        }
    });

    $("#brand-table_filter input[type=search]").attr("placeholder", "search...");

} );