import React, {Fragment, useEffect} from "react";
import {Link, Route, BrowserRouter as Router, Switch} from 'react-router-dom'
import Header from './components/blocks/Header'
import Sidebar from './components/blocks/Sidebar'
import Footer from './components/blocks/Footer'
import ProductList from './components/views/ProductList'
import AddProduct from './components/views/AddProduct'
import Category from './components/views/Category'
import Attribute from './components/views/Attribute'
import Option from './components/views/Option'
import Vendor from './components/views/Vendor'
import Brand from './components/views/Brand'
import globalTranslations from './globalTranslations';
import {renderToStaticMarkup} from "react-dom/server";
import {withLocalize} from "react-localize-redux";
import Price from "./components/views/Price";
import Login from "./components/views/Login";
import {Redirect, useHistory, useLocation} from "react-router";
import createHistory from 'history/createBrowserHistory';


function PrivateRoute({ children, ...rest }) {


    let location=useLocation()
    return (
        <Route {...rest}>
            <Fragment>
                <Header/>
                <div className="content">
                    <div className="container">
                        <Sidebar/>
                        <div className="vd_content-wrapper">
                            {
                            typeof  localStorage.getItem('token')!=='undefined' && localStorage.getItem('token')!=='null'&& localStorage.getItem('token')!==null ? (
                                children
                            ) : (
                                <Redirect
                                    to={{
                                        pathname: "/login",
                                        state: { from: location }
                                    }}
                                />

                            )
                        }
                        </div>

                    </div>
                    <Footer/>
                </div>


            </Fragment>

        </Route>
    );
}


function App(props) {

    useEffect(() => {
        props.initialize({
            languages: [
                {name: "Persian", code: "fa"}
            ],
            translation: globalTranslations,
            options: {
                renderToStaticMarkup,
                renderInnerHtml: true,
                defaultLanguage: "fa"
            }
        });
    }, []);

    const history=createHistory();
    const unlisten = history.listen((location, action) => {
        console.log(action, location.pathname, location.state);
    });


    return (
        <Router history={history}>
            <Switch>
                <PrivateRoute exact  path="/">
                    <ProductList/>
                </PrivateRoute>
                <PrivateRoute exact  path="/productList">
                    <ProductList/>
                </PrivateRoute>
                <Route  path="/login">
                    <Login/>
                </Route>

                <PrivateRoute  path="/addProduct">
                    <AddProduct/>
                </PrivateRoute>
                <PrivateRoute  path="/price">
                    <Price/>
                </PrivateRoute>
                <PrivateRoute  path="/category">
                    <Category/>
                </PrivateRoute>
                <PrivateRoute  path="/attribute">
                    <Attribute/>
                </PrivateRoute>
                <PrivateRoute  path="/option">
                    <Option/>
                </PrivateRoute>
                <PrivateRoute  path="/vendor">
                    <Vendor/>
                </PrivateRoute>
                <PrivateRoute  path="/brand">
                    <Brand/>
                </PrivateRoute>
            </Switch>
        </Router>
    );
}

export default withLocalize(App)
