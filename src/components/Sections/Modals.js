import React from 'react';
import {Modal} from "react-bootstrap";
import Button from "react-bootstrap/es/Button";


export default class Modals extends React.Component {


    render() {
        return  <Modal  show={this.props.show} onHide={()=>{this.props.handleShow(false)}}>
            <Modal.Header className="vd_bg_grey" closeButton>
                <Modal.Title>{this.props.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{this.props.children}</Modal.Body>

        </Modal>
    }
}
Modals.defaultProps={
    show:false
};
