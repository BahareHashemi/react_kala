import React, {Fragment} from 'react'
import DataTable from 'react-data-table-component';
import ButtonGroup from "react-bootstrap/lib/ButtonGroup";
import Button from "react-bootstrap/lib/Button";

function Expanded(props) {
    console.log(props.data,'data expanded');

    const columns = [
        {
            name:'id',
            selector:'id',
            omit:true
        },
        {
            selector: 'name',
            sortable: false,
            style: {
                color: '#202124',
                fontSize: '14px',
                fontWeight: 500,
            }
        },
        {

            cell: (row) => {

                return  <div className="menu-action" >
                    <div
                        onClick={()=>props.handleBtnEdit(row.id)}
                        className="btn menu-icon vd_yellow"> <i
                        className="fa fa-pencil"/> </div>
                    <div
                        onClick={()=>props.handleBtnDelete(row.id)}
                        className="btn menu-icon vd_red"> <i
                        className="fa fa-times"/> </div>

                </div>


            },
            allowOverflow: true,
            button: true,
            width: '200px'
        }
    ];
    const data=props.data.children;

    const customStyles = {

        headCells: {
            style: {
                backgroundColor: '#f2f2f2'

            },
        },
        rows: {
            style: {
                backgroundColor: '#f2f2f2'
            }
        },
    };

    return (
        <div  style={{paddingLeft:"50px",paddingBottom:"50px"}}>
            <DataTable
                columns={columns}
                data={data}
                className="rtl"
                expandableRows
                highlightOnHover
                pagination={false}
                pointerOnHover
                expandableRowDisabled={data=> data.children.length===0}
                expandableIcon={{collapsed:<span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    , expanded:  <span className="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>}}
                expandableRowExpanded={(data)=>data.children.length>0}
                noHeader={true}
                fixedHeader={true}
                TheadComponent={<Fragment>""</Fragment>}
                expandableRowsComponent={<Expanded handleBtnEdit={props.handleBtnEdit}
                                                   handleBtnDelete={props.handleBtnDelete}/>}
            />
        </div>
    )
}

export default Expanded;
