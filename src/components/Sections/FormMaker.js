import React,{useEffect,useState} from 'react'
import * as axios from 'axios';
import PropTypes from 'prop-types';
import Select from 'react-select';
import TagsInput from "./TagsInput";
import '../../customStyle.scss'
import ImageUpload from "./ImageUpload";
import {Translate} from "react-localize-redux";


function FormMaker(props){
    console.log(props,'form maker props')
    const [inputes, setValues] = useState({});
    const [submit, setSubmit]=useState(false);

    useEffect(() => {
       if (submit){
               console.log(inputes,props,'submit true after');
                let data;
               if(props.contain_image){
                   let form = new FormData();
                   if(props.method==="PUT" && props.contain_image){
                       form.append('_method', 'PUT');
                   }
                   Object.keys(inputes).map(i=>{

                       form.append(i, inputes[i]);
                   });

                   data=form;
               }else{
                   data=inputes;

               }
               let headers={};




               axios({
                   method:  props.contain_image?'POST':props.method,
                   url: props.url,
                   data:data,
                   headers:headers
               }).then(res=>{

                   props.getResponse(res.data);

               }).catch(err=>{
                   console.log(err);

               });
           setSubmit(false);

       }

    });

    useEffect(()=>{
        setValues(props.value);
    },[props.value]);


    useEffect(() => {
        setValues({});
        return () => {
            console.log('component will unmount ');
            setValues({});
        }
    },[]);

    const change_values = e => {
        let tmp = {};
        tmp[e.target.name] = e.target.value;

        setValues({ ...inputes, ...tmp });
    };

   function handleChange(is_multi,selectedOption,e){
       console.log(selectedOption,e,is_multi);
       let tmp = {};
       let arr;
       if(is_multi){
           arr=[];
           console.log('is_multi');
           if ( selectedOption!==null)
            selectedOption.map(obj=>arr.push(obj.value));
       }else{
           console.log('is_single');
           arr=selectedOption.value
       }


       tmp[e.name] = arr;
       setValues({ ...inputes, ...tmp });

    };


   function handleFileChange(name,fileInput){

       console.log(fileInput,name,'change file');
       let tmp = {};
       tmp[name] = fileInput;
       console.log(tmp,'tmppppppppppppp');
       setValues({ ...inputes, ...tmp });

   }


    const selectedTags = (tags,name) => {

        console.log('selectedTags',tags,name);
        let tmp=[];
        tmp[name]=tags;
        setValues({ ...inputes, ...tmp });


    };



    console.log(props.inputs,'default value check');

   return (  <div className="form">
                   {props.inputs.map(inp=>{
                           if (inp.type === 'select' && inp.opt!==null) {
                               console.log(inputes,inp.opt,'default value checkkkk');
                               let defaultValue=[];
                               ( inp.opt).map(function(obj){
                                   if( inp.is_multi &&
                                       Object.keys(inputes).length>0
                                       && inputes[inp.name]!==null &&
                                       typeof inputes[inp.name]!=='undefined' &&
                                       inputes[inp.name].includes(obj['value'])){
                                           defaultValue.push(obj)


                                   }else if(typeof inputes!=='undefined' &&
                                       Object.keys(inputes).length>0 &&
                                       inputes[inp.name]!==null &&
                                       inputes[inp.name]===(obj['value'])){
                                            defaultValue.push(obj)


                                   }

                               });
                               console.log(defaultValue,'default value last');



                               return  <div className="row mt-3">
                                           <div className="col-sm-9 text-right controls rtl">
                                               <Select
                                                   name={inp.name}
                                                   onChange={(selected_opt,e)=>handleChange(inp.is_multi,selected_opt,e)}
                                                   options={inp.opt}
                                                   isMulti={inp.is_multi}
                                                   isRtl
                                                   value={defaultValue}
                                               />
                                           </div>
                                   <label className="col-sm-2 text-right control-label">{inp.label}</label>
                               </div>

                           }
                           else if (inp.type === 'text') {

                               return <div className="row mt-3">
                                   <div className="col-sm-9 text-right controls">
                                       <input onChange={change_values}
                                              value={inputes[inp.name]}
                                              className="input-border-btm form-control rtl"
                                              type={inp.type}
                                              name={inp.name}/>
                                   </div>
                                   <label className="col-sm-2 text-right control-label">{inp.label}</label>
                               </div>
                           }
                           else if(inp.type === 'tagsinput'){
                               return <div className="row mt-3">
                                   <div className="col-sm-9 text-right controls rtl">
                                       <TagsInput selectedTags={(selectedTag)=>selectedTags(selectedTag,inp.name)} name={inp.name} />
                                    </div>
                                    <label className="col-sm-2 text-right control-label">{inp.label}</label>
                               </div>
                           }else if(inp.type === 'file'){
                               return <div className="row mt-3">
                                        <div className="col-sm-9 controls rtl">
                                           <ImageUpload value={inputes[inp.name]} setFileForm={(file)=>{handleFileChange(inp.name,file)}} />
                                         </div>
                                         <label className="col-sm-2 text-right control-label">{inp.label}</label>
                                        </div>
                           }else if(inp.type === 'number'){

                               return <div className="row mt-3">
                                   <div className="col-sm-9 text-right controls">
                                       <input onChange={change_values}
                                              value={inputes[inp.name]}
                                              className="input-border-btm form-control rtl"
                                              type={inp.type}
                                              name={inp.name}/>
                                   </div>
                                   <label className="col-sm-2 text-right control-label">{inp.label}</label>
                               </div>
                           }
                       } )}


           <div className="btn vd_bg_cust white-link mt-40" onClick={()=>{setSubmit(true)}}>
               <Translate id="submit"/>
           </div>
       </div> )


   }
FormMaker.propTypes={
    getResponse: PropTypes.func.isRequired,

};
FormMaker.defaultProps={
    contain_image:false
};
export default FormMaker
