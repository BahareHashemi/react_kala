import React, {useRef,useState} from 'react'
import DataTable from 'react-data-table-component';
import * as axios from 'axios';
import conf from "../../config";
import {connect} from "react-redux";
import {opt_reload} from "../../action/options_action";
import {Translate} from "react-localize-redux";


function Inputs(props){

    const inp_ref=useRef();
    const [state,setter]=useState({disabled:true});



    function edit(){
        setter({disabled:false})
    }

    function submit(){
        setter({disabled:true});
        axios({
            method:  "PUT",
            url:  conf.server+"/option/"+props.row.id,
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            },
            data: JSON.stringify({"name":inp_ref.current.value})
        }).then(res=>{

            console.log(res.data,'response edit option')

        }).catch(err=>{
            console.log(err);

        });
    }

    function remove() {
        axios({
            "url":  conf.server+"/option/"+props.row.id,
            "method": "DELETE",
            "timeout": 0,
            "headers": {
                "Accept": "application/json"
            }
        }).then(e=>{
            console.log(e.data);
            props.opt_reload(true)
        })
    }

    return <div className="row">
            <input defaultValue={props.row.name} ref={inp_ref} disabled={state.disabled}    type="text" className="disInput col-2" />
            <button className="btn btn-primary btn-sm  " style={{display: state.disabled ? "inline-block" : "none"}} onClick={edit} >
                <Translate id="edit"/>
            </button>
            <button className="btn btn-success btn-sm  " style={{display:state.disabled?"none":"inline-block"}}  onClick={submit} >
                <Translate id="save"/>
            </button>
            <button className="btn btn-danger btn-sm  " onClick={remove} >
                <Translate id="delete"/>
            </button>
        </div>
}

const InputComponent=connect(undefined,{opt_reload})(Inputs);

function SingleExpand(props) {




    const columns = [
        {
            name:'id',
            selector:'id',
            omit:true,


        },
        {
            selector: 'name',
            sortable: false,
            style: {
                color: '#202124',
                fontSize: '14px',
                fontWeight: 500
            },
            cell:(row)=> {return <InputComponent row={row}/>}
        },

    ];
    const data=props.data.options;



    return (
        <div  style={{paddingLeft:"50px",paddingBottom:"50px"}}>
            <DataTable
                className="rtl"
                columns={columns}
                data={data}
                highlightOnHover
                pagination={false}
                pointerOnHover
                noHeader={true}
                fixedHeader={true}


            />
        </div>
    )
}

export default SingleExpand;
