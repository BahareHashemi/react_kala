import React from 'react'
class ImageUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: ""
            ,showPreview:false};
    }

    componentDidMount() {
        console.log(this.props,'hertttt');
        if(typeof this.props.value!=='undefined'){
            this.setState({imagePreviewUrl:this.props.value,showPreview:true})
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.value!==this.props.value){
            this.setState({imagePreviewUrl:this.props.value,showPreview:true})
        }
    }


    _handleImageChange(e) {

        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result,
                showPreview:true
            });
        };
        this.props.setFileForm(file);

        reader.readAsDataURL(file)
    }

    render() {

        let {imagePreviewUrl} = this.state;  console.log(this.props,imagePreviewUrl);
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img src={imagePreviewUrl} />);
        } else {
            $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
        }

        return (
            <div className="previewComponent">

                    <input className="fileInput"
                           type="file"
                           onChange={(e)=>this._handleImageChange(e)}
                           />

                {this.state.showPreview ?
                <div className="imgPreview">
                {$imagePreview}
                    </div>:''}

            </div>
        )
    }
}

export default ImageUpload
