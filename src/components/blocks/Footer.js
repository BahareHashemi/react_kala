import React from 'react'
import {Translate} from "react-localize-redux";

export  default function Footer () {
    return(
        <footer className="footer-1" id="footer">
            <div className="vd_bottom vd_bg_grey">
                <div className="container">
                    <div className="row">
                        <div className=" col-xs-12">
                            <div className="copyright text-right">
                                <Translate id="footer"/>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </footer>
    )
}
