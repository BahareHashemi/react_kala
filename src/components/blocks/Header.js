import React, {useEffect, useState} from 'react'
import Logo from './../../img/logo.svg';
import LogoName from './../../img/logotype.svg';
import Profilepic from '../../img/profile-pic.png'
import {connect} from "react-redux";
import {Translate} from "react-localize-redux";
import {Link} from "react-router-dom";
import {Redirect, useHistory} from "react-router";

function Header (props) {
    return ( <header className="header-5" id="header">
                    <div className="vd_top-menu-wrapper vd_bg_grey">
                        <div className="container ">
                            <div className="vd_top-nav vd_nav-width  ">
                                <div className="vd_panel-header">
                                    <div className="logo">
                                            <img src={LogoName} className="mr-10" />
                                           <img src={Logo} alt="Logo"/>
                                    </div>

                                    <div className="vd_panel-menu  hidden-sm hidden-xs">

                                    </div>
                                    <div className="vd_panel-menu left-pos visible-sm visible-xs">

                                    </div>
                                    <div className="vd_panel-menu visible-sm visible-xs">
                                        <span className="menu visible-xs" data-action="submenu">
                                            <i className="fa fa-bars"/>
                                        </span>

                                            <span className="menu visible-sm visible-xs" data-action="toggle-navbar-right">
                                                <i className="icon-switch"/>
                                            </span>
                                    </div>

                                    </div>


                            </div>
                            <div className="vd_container">
                                <div className="row">
                                    <div className="col-xs-12">
                                        <div className="vd_mega-menu-wrapper">
                                            <div className="vd_mega-menu pull-right">
                                                <ul className="mega-ul">


                                                    <li id="top-menu-profile" className="profile mega-li">
                                                        <a href="#" className="mega-link" data-action="click-trigger">
                                                    <span className="mega-image">
                                                      <img src={Profilepic} alt="example image"/>
                                                    </span>
                                                            <span className="mega-name">
                                                              {Object.keys(props.user).length>0 && props.user.name}
                                                             <i className="fa fa-caret-down fa-fw"/>
                                                    </span>
                                                        </a>
                                                        <div className="vd_mega-menu-content  width-xs-2  left-xs left-sm"
                                                             data-action="click-target">
                                                            <div className="child-menu">
                                                                <div className="content-list content-menu">
                                                                    <ul className="list-wrapper pd-lr-10">
                                                                        <li>
                                                                            <Link to="/login">
                                                                            <div className="menu-icon"><i
                                                                                className=" fa fa-login"/></div>
                                                                            <div className="menu-text">
                                                                                {Object.keys(props.user).length ===0 && <Translate id={"login"}/>
                                                                                }
                                                                            </div>
                                                                        </Link></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </li>

                                                    <li id="top-menu-settings"
                                                        className="one-big-icon hidden-xs hidden-sm mega-li"
                                                        data-intro="<strong>Toggle Right Navigation </strong><br/>On smaller device such as tablet or phone you can click on the middle content to close the right or left navigation."
                                                        data-step="2" data-position="left">
                                                        <a href="#" className="mega-link" data-action="toggle-navbar-right">
                                                              <span className="mega-icon ">
                                                                 <i className="icon-switch"/>
                                                              </span>

                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                </header>)
}

const m=(state)=>({user:state.user})
export  default  connect(m)(Header)

