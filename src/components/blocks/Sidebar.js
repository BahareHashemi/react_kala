import React from 'react'
import {Link} from 'react-router-dom'
import {Translate} from "react-localize-redux";
import {connect} from "react-redux";
import {logout_user} from "../../action/user_action";

function Sidebar(props) {
    return (
             <div className="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_bg_cust vd_navbar-right">

                 <div className="navbar-menu clearfix">
                    <div className="vd_panel-menu hidden-xs">
                        <span data-original-title="Expand All" data-toggle="tooltip" data-placement="bottom"
                              data-action="expand-all" className="menu"
                              data-intro="<strong>Expand Button</strong><br/>To expand all menu on left navigation menu."
                              data-step="4" >
                        <i className="fa fa-sort-amount-asc"/>
                    </span>
                </div>
                <h3 className="menu-title hide-nav-medium hide-nav-small text-right">
                    <Translate id="menu"/>
                </h3>
                <div className="vd_menu">
                    <ul>
                        <li>
                            <Link push to="#" data-action="click-trigger">
                                <span className="menu-icon"><i className="icon-bag"> </i></span>
                                <span className="menu-text">
                                     <Translate id="product"/>
                                </span>
                                <span className="menu-badge"><span className="badge vd_bg-black-30">
                                    <i className="fa fa-angle-down"/></span></span>
                            </Link>

                            <div className="child-menu" data-action="click-target">
                                <ul>
                                    <li>
                                        <Link push to="/productList"> <span className="menu-text">
                                            <Translate id="productList"/>
                                        </span></Link>

                                    </li>
                                    <li>
                                        <Link push to="/addProduct">
                                            <span className="menu-text">
                                                 <Translate id="addProduct"/>
                                            </span>
                                        </Link>
                                    </li>

                                </ul>
                            </div>
                          </li>
                        <li>
                            <Link push to="/price">
                                <span className="menu-icon"><i className="fa fa-money"/></span>
                                <span className="menu-text">
                                    <Translate id="price"/>
                                </span>
                            </Link>
                        </li>
                        <li>
                              <Link to="#" data-action="click-trigger">
                                    <span className="menu-icon"><i className="icon-folder"> </i></span>
                                    <span className="menu-text"> <Translate id="category"/></span>
                                    <span className="menu-badge"><span className="badge vd_bg-black-30">
                                        <i className="fa fa-angle-down"/></span></span>
                                </Link>
                                <div className="child-menu" data-action="click-target">
                                    <ul>
                                        <li>
                                            <Link push to="/category">
                                            <span className="menu-text"> <Translate id="categories"/></span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link push to="/attribute">
                                                <span className="menu-text"> <Translate id="attribute"/></span>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link push to="/option">
                                                <span className="menu-text"> <Translate id="option"/></span>
                                            </Link>
                                        </li>


                                     </ul>
                                  </div>
                        </li>
                        <li>
                            <Link push to="/vendor">
                                <span className="menu-icon"><i className="icon-users"/></span>
                                <span className="menu-text"> <Translate id="vendor"/></span>
                            </Link>
                        </li>
                        <li>
                            <Link push to="/brand">
                                <span className="menu-icon"><i className="icon-pictures"/></span>
                                <span className="menu-text"> <Translate id="brand"/></span>
                            </Link>
                        </li>

                     </ul>

                 </div>
                 </div>
                  <div className="navbar-spacing clearfix">
                    </div>
                  <div className="vd_menu vd_navbar-bottom-widget">
                        <ul>
                            <li>
                                <Link push to="/">
                                    <span className="menu-icon"><i className="fa fa-sign-out"/></span>
                                    <span className="menu-text" onClick={props.logout_user}> <Translate id="logout"/></span>
                                </Link>

                            </li>
                        </ul>
                    </div>
                  </div>

    )
}


export default connect(undefined,{logout_user})(Sidebar)
