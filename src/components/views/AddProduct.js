import React, {Fragment, useEffect, useState} from 'react'
import Select from 'react-select';
import ImageUpload from "../Sections/ImageUpload";
import * as axios from "axios/index";
import conf from "../../config";
import {maker_opt} from "../helper";
import {func} from "prop-types";
import {Translate} from "react-localize-redux";
import {useHistory} from "react-router";


function Information(props){
    const [info,setInfo]=useState({name:null,eng_name:null,brand_id:null,description:""});
    const [brands,setBrandLs]=useState([]);


    useEffect(()=>{
        loadBrand();
    },[]);



    function  loadBrand(){
        var settings = {
            "url": conf.server+"/brand",
            "method": "GET",
            "timeout": 0,
        };

        axios(settings).then((response)=> {
            let br= maker_opt(response.data.data,"name","id");
            setBrandLs(br);
        });
    }

    function handleChange(value,name) {
        setInfo({...info,[name]:value})
        props.setter({...info,[name]:value})

    }
    return(
        <Fragment>

            <div className="form-group row ">
                <div className="col-md-10 col-md-offset-3">
                    <div className="col-sm-7  ">
                        <input type="text"
                               required
                               name="name"
                               value={info["name"]}
                               onChange={(e)=>{handleChange(e.target.value,"name")}}
                               className="updateCurrentText"
                               id="name"/>

                    </div>
                    <label htmlFor="name"
                           className="control-label col-sm-2 required"> <Translate id={"productName"}/>
                    </label>
                </div>
            </div>

            <div className="form-group row ">
                <div className="col-md-10 col-md-offset-3">
                    <div className="col-sm-7  ">
                        <input type="text"
                               required
                               name="eng_name"
                               className="updateCurrentText"
                               id="eng_name"
                               value={info["eng_name"]}
                               onChange={(e)=>handleChange(e.target.value,"eng_name")}
                              />

                    </div>
                    <label htmlFor="eng_name"
                           className="control-label col-sm-2 required"> <Translate id={"productEngName"}/>
                    </label>
                </div>
            </div>

            <div className="row mt-3">
                <div className="col-md-10 col-md-offset-3">
                    <div className="col-sm-7 text-right controls rtl">
                        <Select
                            onChange={(selected,e)=>handleChange(selected["value"],"brand_id")}
                            options={brands}
                            isRtl
                            isMulti={false}

                        />
                    </div>
                    <label className="col-sm-2 text-right control-label"><Translate id={"brandSelect"}/></label>
                </div>
            </div>

            <div className="form-group row ">
                <div className="col-md-10 col-md-offset-3">
                    <div className="col-sm-7">
                     <textarea name="description" onChange={(e)=>handleChange(e.target.value,"description")} id="description"/>
                    </div>
                    <label htmlFor="description"
                           className="control-label col-sm-2 required"> <Translate id={"description"}/>
                    </label>
                </div>
            </div>
        </Fragment>
    )
}



function Images(props){
    const [img,setImg]=useState({file:undefined,preview:undefined});
    const [rows,setRows]=useState([]);
    const [cover,setCover]=useState(0);

    useEffect(()=> {
        let j=0;
        rows.map((obj,index)=>{
            if(cover===index){
                props.setter({pic:obj.file})
            }else{
                let name=`pics[${j}]`;
                props.setter({[name]:obj.file})
                j++;
            }
        })
    },[rows])


    function handleFileChange(file){
        let reader = new FileReader();

        reader.onloadend = () => {
            setImg({file:file,preview:reader.result});
        };
        reader.readAsDataURL(file)

    }


    function addRow() {
      if (typeof img.file!=='undefined'){
          console.log("imgggg",img);
          setRows([...rows,img]);
          setImg({file:undefined,preview:undefined})
      }

    }



    function deleteImg(index){
        console.log('delete',index);
        rows.splice(parseInt(index),1);
        let tmp=[...rows]
        setRows(tmp);
        console.log('deleteddd',index,rows);
    }

    return(<Fragment>

            <div className="form-horizontal">
                <div className="row">
                    <div className="col-md-10 col-md-offset-3">
                        <div className="col-sm-3">
                            <a onClick={()=>addRow()}
                                className="btn vd_btn vd_bg_cust btn-sm save-btn pull-left"><i
                                className="pl-1 fa fa-save append-icon"
                                 />
                                <Translate id={"uploadImage"}/></a>
                        </div>
                        <div className="col-sm-6 controls rtl">
                            <ImageUpload  setFileForm={(file)=>{handleFileChange(file)}}/>
                        </div>
                    </div>
                </div>
                <table id="imageTable" className="table">
                    <thead>
                    <tr className="nodrag nodrop">

                        <th className="fixed-width-lg" style={{width:'80px'}}><span
                            className="title_box"><Translate id={"picTable"}/></span></th>
                        <th className="fixed-width-xs"><span
                            className="title_box"><Translate id={"cover"}/></span></th>
                        <th/>

                    </tr>
                    </thead>
                    <tbody id="imageList">
                    {rows.map((obj,index)=>{

                        return <tr>
                                    <td>
                                        <img
                                        src={obj.preview}
                                        alt="product image"/>
                                    </td>

                                    <td className="cover">
                                        <div className="checkbox">
                                            <input
                                                type="radio"
                                                checked={cover===index}
                                                value={index}
                                                onChange={()=>setCover(index)}
                                                name="cover"
                                            />
                                        </div>


                                    </td>
                                    <td><button
                                        onClick={()=>deleteImg(index)}
                                        className="delete_product_image pull-right btn vd_btn vd_bg-yellow btn-sm"
                                       > <i
                                        className="icon-trash append-icon"


                                    /> <Translate id={"delete"}/> </button></td>
                                </tr>})}
                    </tbody>
                </table>
            </div>
        </Fragment>)
}



function CategoryTab(props){
    const [selected,selectCat]=useState(null);

    const [cat_opt,setCat]=useState([]);


    useEffect(()=>{
        loadCategory();
    },[]);

    function loadCategory(){
        var settings = {
            "url": conf.server+"/category",
            "method": "GET",
            "timeout": 0,
        };

        axios(settings).then( (response)=>{
            console.log(response,'cat opt category tab')
            setCat(response.data.data)
        });
    }

    function onClickCat(cat){
        console.log(cat,'selected cat')
        selectCat(cat);
        props.setter({"category_id":cat.id})

    }

    function selectOptions(selectedOption,index) {

    if(selectedOption!==null){
        let name=`options[${index}]`;
        props.setter({[name]:selectedOption.value})
    }


    }



    return(
        <Fragment className="z-index">
            <div className="form-group">
                <div className="col-md-8 col-md-offset-4">
                <div className="col-lg-10 mb-5">
                    <div className="categories-scroll">
                        <ul  style={{marginLeft: "-15px",direction:"rtl"}}>
                            {typeof cat_opt !=='undefined' && cat_opt.map(i => {

                                if (i['parent_id'] === null) {
                                    let children = []
                                    if (typeof i['children'] !=='undefined' && i['children'].length !== 0) {
                                        i['children'].map(j => {
                                            children.push(<li>
                                                <div className="rtl checkbox">
                                                    <input
                                                        type="radio"
                                                        name="category"
                                                        id={"chi" + j['id']}
                                                        value={j['id']}
                                                        onChange={()=>onClickCat(j)}/>
                                                    <label htmlFor={"chi" + j['id']}>
                                                        {j['name']}
                                                    </label>


                                                </div>
                                            </li>)
                                        })
                                    }
                                    console.log(children, 'children')

                                    return (<li>
                                        <div className="rtl checkbox">
                                            <input type="radio"
                                                   name="category"
                                                   id={"par" + i['id']}
                                                   value={i['id']}
                                                   onChange={()=>onClickCat(i)}
                                            />
                                            <label
                                                htmlFor={"par" + i['id']}> {i['name']} </label>


                                        </div>
                                        <ul>
                                            {children}
                                        </ul>
                                    </li>)


                                }
                            })}
                        </ul>
                    </div>
                </div>
                <label className="control-label col-sm-2">
                    <Translate id={"category"}/>
                </label>

            </div>

             </div>
            {console.log("selected cat",selected,selected!==null && typeof selected['attributes']!=='undefined')}
            { selected!==null &&
            typeof selected['attributes']!=='undefined' &&
            selected['attributes'].map(function(attr,index) {
                let options=typeof attr['options'] !=='undefined' ?maker_opt(attr['options'],"name","id"):[];
                console.log(options,'selected cat options')
                return <div className="row mt-3 my-2">
                        <div className="col-md-7 col-md-offset-5">
                            <div className="col-sm-7 text-right controls rtl">
                                <Select
                                    options={options}
                                    isRtl
                                    onChange={(selected)=>selectOptions(selected,index)}
                                    isDisabled={options.length===0}
                                />
                            </div>
                            <label className="col-sm-2 text-right control-label">{attr.name}</label>
                        </div>
                    </div>
            })
            }



        </Fragment>)
}


export default function AddProduct (){

    const [inputs,setData]=useState({})

    let history=useHistory();

    function setInput(obj) {
        console.log(inputs,'object is not iterablw')
            setData({...inputs,...obj})
    }

    function saveProduct(){
        var form = new FormData();
        Object.keys(inputs).forEach(function(key){
            form.append(key,inputs[key] );
        })



        var settings = {
            "url": conf.server+"/product",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Accept": "application/json"
            },
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": form
        };



        axios(settings).then(function (response) {
            console.log(response,'add product response');
            history.push("/productList")
        }).catch(e=>{
            alert("خطا در ثبت محصول دوباره سعی کنید"+JSON.stringify(e))
        });
    }



    return(
        <div className="vd_content-wrapper" style={{minHeight: "1059px"}}>
            <div className="vd_container" style={{minHeight: "1059px"}}>
                <div className="vd_content clearfix">
                    <div className="vd_head-section clearfix" style={{border:"0"}}>
                        <div className="vd_panel-header">
                            <h3 className="main-title pull-right">
                                <Translate id={"addProduct"}/>
                            </h3>
                        </div>
                    </div>

                    <div className="vd_content-section clearfix" style={{border:"0"}}>
                        <div className="row">
                            <div className="col-sm-3 col-md-4 col-lg-3" style={{padding:'0'}}>
                                <div className="form-wizard condensed mgbt-xs-20">
                                    <ul className="nav nav-tabs nav-stacked">
                                        <li className="active"><a href="#tabinfo" data-toggle="tab"> <i
                                            className="fa fa-info-circle append-icon"/><Translate id={"information"}/></a></li>
                                        <li><a href="#tabimg" data-toggle="tab"> <i
                                            className="fa fa-book append-icon"/> <Translate id={"pictures"}/> </a></li>
                                        <li><a href="#tabopt" data-toggle="tab"> <i
                                            className="fa fa-folder-open append-icon"/> <Translate id={"category"}/></a></li>


                                    </ul>
                                </div>
                            </div>
                            <div className="col-sm-9 col-md-8 col-lg-9 tab-right rtl" style={{padding:'0'}}>
                                <div className="panel widget panel-bd-left light-widget">

                                    <div className="panel-body" style={{paddingTop:"0"}}>
                                        <div className="tab-content no-bd mg-xs-50 ">
                                            <div id="tabinfo" className="tab-pane active">
                                                <Information setter={setInput} />
                                            </div>

                                            <div id="tabimg" className="tab-pane">
                                               <Images setter={setInput}  />
                                            </div>

                                            <div id="tabopt" className="tab-pane">
                                               <CategoryTab setter={setInput} />

                                            </div>


                                            <div className="vd_panel-menu pull-left mt-5" >
                                                <div>
                                                    <button
                                                        onClick={saveProduct}
                                                        className="btn vd_btn vd_bg_cust btn-sm save-btn pull-left mr-1">
                                                        <i  className="fa fa-save append-icon"/> <Translate id={"save"}/>
                                                    </button>

                                                </div>
                                            </div>


                                        </div>


                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    )

}




