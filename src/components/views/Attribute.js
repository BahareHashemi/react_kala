import React, {Fragment} from 'react';
import Button from "react-bootstrap/es/Button";
import DataTable from 'react-data-table-component';
import Modals from "../Sections/Modals";
import conf from "../../config";
import FormMaker from "../Sections/FormMaker";
import Skeleton from 'react-loading-skeleton';
import * as axios from 'axios';
import ButtonGroup from "react-bootstrap/es/ButtonGroup";
import {maker_opt} from "../helper";
import {Translate} from "react-localize-redux";


class Attribute extends React.Component {
    constructor(){
        super();
        this.state={
            show:false,
            isHoveredIcon:false,
            show_skeleton:true,
            items: [],
            editedItem:{},
            modalTitle:<Translate id={"addAttribute"}/>,
            url_form:conf.server+"/attribute",
            edit_id:0,
            method_form:"POST",
            att_delete:0,
            delete_show:false
        };
        this.toggleHover=this.toggleHover.bind(this);
        this.loadAttributes=this.loadAttributes.bind(this);
        this.handleBtnEdit=this.handleBtnEdit.bind(this);
        this.setDeletedId=this.setDeletedId.bind(this);
        this.DeleteAttribute=this.DeleteAttribute.bind(this);

    }
    componentDidMount(){

        this.loadAttributes();
    }

    hidemodal(show,url=conf.server+"/attribute",method="POST"){

        this.setState({
            show:show,
            url_form:url,
            method_form:method,
            modalTitle:<Translate id={"addAttribute"}/>
        })
    }

    toggleHover(){
        this.setState(prevState => ({isHoveredIcon: !prevState.isHoveredIcon}));
    }


    loadAttributes(){

        var settings = {
            "url": conf.server+"/attribute",
            "method": "GET",
            "timeout": 0,
        };
        let that=this;
        axios(settings).then(function (response) {
            that.setState({items:response.data.data,show_skeleton:false})
        });
    }

    getResponse(){

        this.setState({
            show:false,
            modalTitle:<Translate id={"addAttribute"}/>
        });
        this.loadAttributes()
    }

    handleBtnEdit(id){
        console.log('Selected edit Row: ', id);
        this.setState({
            show:true,
            modalTitle:<Translate id={"editAttribute"}/>
        })
        axios({
            method: 'GET',
            url: conf.server+"/attribute/"+id,

        }).then(res=>{
            console.log('resItem',res.data)
            this.setState({
                editedItem:res.data.data
            })

            console.log('resItem',res.data);
            let att=res.data.data;
            let editedItem={};
            editedItem['name']=att['name'];


            this.setState({
                editedItem,
                url_form:conf.server+"/attribute/"+id,
                method_form:"PUT"
            })

        })
    }

    setDeletedId(id){
        this.setState({delete_show:true,att_delete:id});
    }

    DeleteAttribute(){
        axios({
            "url": conf.server+"/attribute/"+this.state.att_delete,
            "method": "DELETE",
            "timeout": 0,
        }).then( (response) =>{
            this.setState({att_delete:0, delete_show:false});
            this.loadAttributes();
        });
    }

    render() {

        const data=this.state.items;
        const columns = [
            {
                name:'id',
                selector:'id',
                omit:true
            },
            {

                name: <Translate id="attributeName"/>,
                selector: 'name',
                sortable: true,
                style: {
                    color: '#202124',
                    fontSize: '14px',
                    fontWeight: 500,
                }
            },
            {

                cell: (row) => {

                    return  <div className="menu-action" >
                        <div
                            onClick={()=>this.handleBtnEdit(row.id)}
                            className="btn menu-icon vd_yellow"> <i
                            className="fa fa-pencil"/> </div>
                        <div
                            onClick={()=>this.setDeletedId(row.id)}
                            className="btn menu-icon vd_red"> <i
                            className="fa fa-times"/> </div>

                    </div>




                },
                allowOverflow: true,
                button: true,
                width: '200px'
            }
        ];

        return (
            <div className="vd_content-wrapper">
                <div className="vd_container">
                    <div className="vd_content clearfix">
                        <div className="vd_title-section clearfix">
                            <div className="vd_content-section clearfix">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="panel widget light-widget">

                                            <div className="panel-body table-responsive">
                                                <div className="btn vd_bg_cust white-link pull-right" onClick={()=>{this.setState({show:true})}}>
                                                    <Translate id="addAttribute"/>
                                                    <i className="icon-add-to-list white-link ml-2"/>

                                                </div>
                                                {this.state.show_skeleton === true?
                                                    <Skeleton count={10}/>:
                                                    <DataTable
                                                        title="Attribute"
                                                        className="rtl"
                                                        columns={columns}
                                                        data={data}
                                                        highlightOnHover
                                                        pagination
                                                        pointerOnHover
                                                        defaultSortField="name"
                                                        noHeader
                                                        compact={true}
                                                    />
                                                }
                                            </div>

                                            <Modals  title={this.state.modalTitle} show={this.state.show} handleShow={this.hidemodal.bind(this)}>
                                                <FormMaker
                                                    url={this.state.url_form}
                                                    getResponse={this.getResponse.bind(this)}
                                                    value={this.state.editedItem}
                                                    contain_image={false}
                                                    inputs={
                                                        [
                                                            {name:"name",type:"text",
                                                                label:<Translate id={"name"}/>}

                                                        ]}
                                                    edit_id={this.state.edit_id}
                                                    method={this.state.method_form}
                                                />
                                            </Modals>
                                            <Modals title={ <Translate id="delete"/>}  show={this.state.delete_show} handleShow={(show)=>{this.setState({delete_show:show,att_delete:0})}}>
                                                <div className="d-flex text-right justify-content-end ">
                                                    <Translate id="welcome"/>

                                                </div>
                                                <div className="d-flex justify-content-center mt-4">
                                                    <div className="btn btn-danger" onClick={this.DeleteAttribute.bind(this)}>
                                                        <Translate id="delete"/>
                                                    </div>
                                                </div>
                                            </Modals>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default Attribute
