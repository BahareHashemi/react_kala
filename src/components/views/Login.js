import React, {useEffect, useState} from "react";
import  LogoName from "../svg/logotype.svg";
import  Logo from "../svg/logo.svg";
import {Translate} from "react-localize-redux";
import * as axios from "axios";
import { useHistory } from "react-router-dom";
import conf from "../../config";
import {user_setter} from "../../action/user_action";
import {connect} from "react-redux";


function Login(props) {

    const [inputs,setter]=useState({});
    useEffect((() => localStorage.setItem('token',null)),[])


    function setValue(obj){
        setter({...inputs,...obj})
    }

    function login(){

        var form = new FormData();
        Object.keys(inputs).map(key=>{
            form.append(key, inputs[key]);
        })


        var settings = {
            "url": conf.server+"/admin/login",
            "method": "POST",
            "timeout": 0,
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": form
        };

        axios(settings).then(function (response) {
            localStorage.setItem("token",response.data.token);

            window.location.href="/productList"
        });

    }

    return (

        <div className="full-layout no-nav-left no-nav-right  nav-top-fixed background-login     responsive login-layout   clearfix breakpoint-975 sticky-menu-active">
            <div className="content">

                <div className="container">
                    <div className="vd_container">
                        <div className="vd_content clearfix">
                            <div className="vd_content-section clearfix">
                                <div className="vd_login-page">
                                    <div className="heading clearfix">
                                        <div className="logo">
                                            <h2 className="mgbt-xs-5"/>
                                            <div className="logo">
                                                <img src={LogoName} className="mr-10" />
                                                <img src={Logo} alt="Logo"/>
                                            </div>

                                        </div>

                                    </div>
                                    <div className="panel widget">
                                        <div className="panel-body">
                                            <div className="login-icon entypo-icon"><i className="icon-key"></i></div>
                                            <div className="form-horizontal" id="login-form" action="#" role="form">
                                                <div className="alert alert-danger vd_hidden">
                                                    <button type="button" className="close" data-dismiss="alert"
                                                            aria-hidden="true">
                                                        <i className="icon-cross"/>
                                                    </button>
                                                    <span className="vd_alert-icon">
                                                        <i className="fa fa-exclamation-circle vd_red"/>
                                                    </span><strong>Oh
                                                    snap!</strong> Change a few things up and try submitting again.
                                                </div>
                                                <div className="alert alert-success vd_hidden">
                                                    <button type="button" className="close" data-dismiss="alert"
                                                            aria-hidden="true">
                                                        <i className="icon-cross"/>
                                                    </button>
                                                    <span className="vd_alert-icon">
                                                        <i className="fa fa-check-circle vd_green"/></span><strong>Well
                                                    done!</strong>.
                                                </div>
                                                <div className="form-group  mgbt-xs-20">
                                                    <div className="col-md-12">
                                                        <div className="label-wrapper sr-only">
                                                            <label className="control-label" htmlFor="email"><Translate id="email"/></label>
                                                        </div>
                                                        <div className="vd_input-wrapper" id="email-input-wrapper">
                                                         <span
                                                            className="menu-icon">
                                                            <i className="fa fa-envelope"/>
                                                        </span>
                                                            <input type="email"
                                                                   placeholder="ایمیل"
                                                                   onChange={(e)=>setValue({email:e.target.value})}
                                                                   id="email"
                                                                   name="email"
                                                                   className="required" required/>
                                                        </div>
                                                        <div className="label-wrapper">
                                                            <label className="control-label sr-only"
                                                                   htmlFor="password"><Translate id="password"/></label>
                                                        </div>
                                                        <div className="vd_input-wrapper" id="password-input-wrapper">
                                                        <span className="menu-icon">
                                                            <i className="fa fa-lock"/>
                                                        </span>
                                                            <input
                                                                type="password"
                                                                placeholder={"پسورد"}
                                                                id="password"
                                                                name="password"
                                                                className="required"
                                                                onChange= {(e)=>setValue({password:e.target.value})}
                                                                required/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="vd_login-error" className="alert alert-danger hidden">
                                                    <i  className="fa fa-exclamation-circle fa-fw"/>
                                                    Please fill the
                                                    necessary field
                                                </div>
                                                <div className="form-group">
                                                    <div className="col-md-12 text-center mgbt-xs-5">
                                                        <button
                                                            className="btn vd_bg-green vd_white width-100"
                                                            onClick={login}
                                                            id="login-submit">
                                                            <Translate id="login"/>
                                                        </button>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="row">
                                                            <div className="col-xs-6">
                                                                <div className="vd_checkbox">

                                                                </div>
                                                            </div>
                                                            <div className="col-xs-6 text-right">
                                                                <input type="checkbox" id="checkbox-1" value="1"/>
                                                                <label htmlFor="checkbox-1"><Translate id={"remember"}/></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
       )
}

export default connect(undefined,{user_setter})(Login)
