import React from 'react';
import Button from "react-bootstrap/es/Button";
import DataTable from 'react-data-table-component';
import Modals from "../Sections/Modals";
import conf from "../../config";
import FormMaker from "../Sections/FormMaker";
import Skeleton from 'react-loading-skeleton';
import * as axios from 'axios';
import ButtonGroup from "react-bootstrap/es/ButtonGroup";
import {maker_opt} from "../helper";
import {Translate} from "react-localize-redux";


class Brand extends React.Component {
    constructor(){
        super();
        this.state={
            show:false,
            isHoveredIcon:false,
            show_skeleton:true,
            items: [],
            editedItem:{},
            modalTitle:<Translate id={"addBrand"}/>,
            url_form:conf.server+"/brand",
            edit_id:0,
            method_form:"POST",
            br_delete:0,
            delete_show:false
        };
        this.toggleHover=this.toggleHover.bind(this);
        this.loadBrands=this.loadBrands.bind(this);
        this.handleBtnEdit=this.handleBtnEdit.bind(this);
        this.setDeletedId=this.setDeletedId.bind(this);
        this.DeleteBrand=this.DeleteBrand.bind(this);

    }
    componentDidMount(){

        this.loadBrands();
    }

    hidemodal(show,url=conf.server+"/brand",method="POST"){

        this.setState({
            show:show,
            url_form:url,
            method_form:method,
            modalTitle:<Translate id={"addBrand"}/>
        })
    }

    toggleHover(){
        this.setState(prevState => ({isHoveredIcon: !prevState.isHoveredIcon}));
    }


    loadBrands(){

        var settings = {
            "url": conf.server+"/brand",
            "method": "GET",
            "timeout": 0,
        };
        let that=this;
        axios(settings).then(function (response) {
            that.setState({items:response.data.data,show_skeleton:false})
        });
    }

    getResponse(){

        this.setState({
            show:false,
            modalTitle:<Translate id={"addBrand"}/>});
        this.loadBrands()
    }

    handleBtnEdit(id){
        console.log('Selected edit Row: ', id);
        this.setState({
            show:true,
            modalTitle:<Translate id={"editBrand"}/>,
        })
        axios({
            method: 'GET',
            url: conf.server+"/brand/"+id,

        }).then(res=>{
            console.log('resItem',res.data)
            this.setState({
                editedItem:res.data.data
            })

            console.log('resItem',res.data);
            let br=res.data.data;
            let editedItem={};
            editedItem['name']=br['name'];
            editedItem['pic']=br['pic'].split(" ")[0];

            this.setState({
                editedItem,
                url_form:conf.server+"/brand/"+id,
                method_form:"PUT"
            })

        })
    }

    setDeletedId(id){
        this.setState({delete_show:true,br_delete:id});
    }

    DeleteBrand(){
        axios({
            "url": conf.server+"/brand/"+this.state.br_delete,
            "method": "DELETE",
            "timeout": 0,
        }).then( (response) =>{
            this.setState({br_delete:0, delete_show:false});
            this.loadBrands();
        });
    }

    render() {

        const data=this.state.items;
        const columns = [
            {
                name:'id',
                selector:'id',
                omit:true
            },
            {

                name: <Translate id="brandName"/>,
                selector: 'name',
                sortable: true,
                style: {
                    color: '#202124',
                    fontSize: '14px',
                    fontWeight: 500,
                }
            },
            {
               name:'pic' ,
                selector:'pic',
                omit:true

            },
            {

                name: <Translate id="picTable"/>,
                selector: 'picture',
                cell: (row)=> <img className="p-1" style={{borderRadius:"1rem"}} width="84px" height="56px" src={row.pic}/>



            },
            {

                cell: (row) => {

                    return  <div className="menu-action" >
                        <div
                            onClick={()=>this.handleBtnEdit(row.id)}
                            className="btn menu-icon vd_yellow"> <i
                            className="fa fa-pencil"/> </div>
                        <div
                            onClick={()=>this.setDeletedId(row.id)}
                            className="btn menu-icon vd_red"> <i
                            className="fa fa-times"/> </div>

                    </div>


                },
                allowOverflow: true,
                button: true,
                width: '200px'
            }
        ];



        return (
            <div className="vd_content-wrapper">
                <div className="vd_container">
                    <div className="vd_content clearfix">
                        <div className="vd_title-section clearfix">
                            <div className="vd_content-section clearfix">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="panel widget light-widget">
                                            <div className="panel-body table-responsive">
                                                <div className="btn vd_bg_cust white-link pull-right" onClick={()=>{this.setState({show:true})}}>
                                                    <Translate id="addBrand"/>
                                                    <i className="icon-add-to-list white-link ml-2"/>

                                                </div>
                                                {this.state.show_skeleton === true?
                                                    <Skeleton count={10}/>:
                                                    <DataTable
                                                        title="Brand"
                                                        className="rtl"
                                                        columns={columns}
                                                        data={data}
                                                        highlightOnHover
                                                        pagination
                                                        pointerOnHover
                                                        defaultSortField="name"
                                                        noHeader
                                                    />
                                                }
                                            </div>

                                            <Modals  title={this.state.modalTitle} show={this.state.show} handleShow={this.hidemodal.bind(this)}>
                                                <FormMaker
                                                    url={this.state.url_form}
                                                    getResponse={this.getResponse.bind(this)}
                                                    value={this.state.editedItem}
                                                    contain_image={true}
                                                    inputs={
                                                        [
                                                            {name:"name",type:"text", label:<Translate id={"name"}/>},
                                                            {type:"file",  label:<Translate id={"picTable"}/>,name:"pic"}
                                                        ]}
                                                    edit_id={this.state.edit_id}
                                                    method={this.state.method_form}
                                                />
                                            </Modals>
                                            <Modals title={ <Translate id="delete"/>}  show={this.state.delete_show} handleShow={(show)=>{this.setState({delete_show:show,br_delete:0})}}>
                                                <div className="d-flex justify-content-end text-right">
                                                    <Translate id="welcome"/>

                                                </div>
                                                <div className="d-flex justify-content-center mt-4">
                                                    <div className="btn btn-danger" onClick={this.DeleteBrand.bind(this)}>
                                                        <Translate id="delete"/>
                                                    </div>
                                                </div>
                                            </Modals>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default Brand
