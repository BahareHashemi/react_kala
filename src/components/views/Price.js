import React from 'react';
import Button from "react-bootstrap/es/Button";
import DataTable from 'react-data-table-component';
import Modals from "../Sections/Modals";
import conf from "../../config";
import FormMaker from "../Sections/FormMaker";
import Skeleton from 'react-loading-skeleton';
import * as axios from 'axios';
import ButtonGroup from "react-bootstrap/es/ButtonGroup";
import {maker_opt} from "../helper";
import {Translate} from "react-localize-redux";


class Price extends React.Component {
    constructor(){
        super();
        this.state={
            show:false,
            isHoveredIcon:false,
            show_skeleton:true,
            items: [],
            editedItem:{},
            pro_opt:[],
            ven_opt:[],
            modalTitle:<Translate id="addPrice"/>,
            url_form:conf.server+"/price",
            edit_id:0,
            method_form:"POST",
            pr_delete:0,
            delete_show:false
        };

        this.loadPrice=this.loadPrice.bind(this);
        this.loadProducts=this.loadProducts.bind(this);
        this.loadVendors=this.loadVendors.bind(this);
        this.handleBtnEdit=this.handleBtnEdit.bind(this);
        this.setDeletedId=this.setDeletedId.bind(this);
        this.DeletePrice=this.DeletePrice.bind(this);

    }
    componentDidMount(){

        this.loadPrice();
        this.loadVendors();
        this.loadProducts();
    }

    hidemodal(show,url=conf.server+"/vendor",method="POST"){

        this.setState({
            show:show,
            url_form:url,
            method_form:method
        })
    }


    loadPrice(){

        var settings = {
            "url": conf.server+"/price",
            "method": "GET",
            "timeout": 0,
        };
        let that=this;
        axios(settings).then(function (response) {


            let res=response.data.data;
            let data=[];
            let tmp={};
            Object.values(res).map((i)=>{
                tmp['id']=i['id'];
                tmp['product_id']=i['product']!==null ?['product']['name']:<Translate id={"nn"}/>;
                tmp['vendor_id']=i['vendor']!==null ?i['vendor']['name']:<Translate id={"nn"}/>;
                tmp['single_price']=i['single_price'];
                tmp['bulk_price']=i['bulk_price'];
                tmp['minimum_bulk']=i['minimum_bulk'];
                    data.push(tmp);

                tmp={}
            });


            that.setState({items:data,show_skeleton:false})
        });
    }

    loadProducts(){
        var settings = {
            "url": conf.server+"/product",
            "method": "GET",
            "headers": {
                "Accept": "application/json"
            },
            "timeout": 0,
        };
        axios(settings).then( (response)=> {
            let options= maker_opt(response.data.data,"name","id");
            this.setState({pro_opt:options});
        });

    }

    loadVendors(){
        var settings = {
            "url": conf.server+"/vendor",
            "method": "GET",
            "headers": {
                "Accept": "application/json"
            },
            "timeout": 0,
        };
        axios(settings).then( (response)=> {
            let options= maker_opt(response.data.data,"name","id");
            this.setState({ven_opt:options});
        });

    }

    getResponse(){

        this.setState({show:false});
        this.loadPrice()
    }

    handleBtnEdit(id){
        console.log('Selected edit Row: ', id);
        this.setState({
            show:true,
            modalTitle:'Edit Price'
        })
        axios({
            method: 'GET',
            url: conf.server+"/price/"+id,

        }).then(res=>{
            console.log('resItem',res.data)
            this.setState({
                editedItem:res.data.data
            })

            console.log('resItem',res.data);
            let pr=res.data.data;
            let editedItem={};
            editedItem['product_id']=pr['product']['name'];
            editedItem['vendor_id']=pr['vendor']['name'];
            editedItem['single_price']=pr['single_price'];
            editedItem['bulk_price']=pr['bulk_price'];
            editedItem['minimum_bulk']=pr['minimum_bulk'];


            this.setState({
                editedItem,
                url_form:conf.server+"/price/"+id,
                method_form:"PUT"
            })

        })
    }

    setDeletedId(id){
        this.setState({delete_show:true,pr_delete:id});
    }

    DeletePrice(){
        axios({
            "url": conf.server+"/price/"+this.state.pr_delete,
            "method": "DELETE",
            "timeout": 0,
        }).then( (response) =>{
            this.setState({pr_delete:0, delete_show:false});
            this.loadPrice();
        });
    }

    render() {

        const data=this.state.items;

        const columns = [
            {
                name:'id',
                selector:'id',
                omit:true
            },
            {

                name:  <Translate id="productName"/>,
                selector: 'product_id',
                sortable: true,
                style: {
                    color: '#202124',
                    fontSize: '14px',
                    fontWeight: 500,
                }
            },
            {
                name:  <Translate id="vendorName"/>,
                selector: 'vendor_id'
            },
            {
                name:  <Translate id="singlePrice"/>,
                selector: 'single_price'
            },
            {
                name: <Translate id="bulkPrice"/> ,
                selector:'bulk_price'


            },
            {

                name:  <Translate id="minBulk"/>,
                selector: 'minimum_bulk'


            },
            {

                cell: (row) => {

                    return <div className="menu-action" >

                        <div
                            onClick={()=>this.setDeletedId(row.id)}
                            className="btn menu-icon vd_red"> <i
                            className="fa fa-times"/> </div>

                    </div>


                },
                allowOverflow: true,
                button: true,
                width: '200px'
            }
        ];

        return (
            <div className="vd_content-wrapper">
                <div className="vd_container">
                    <div className="vd_content clearfix">
                        <div className="vd_title-section clearfix">
                            <div className="vd_content-section clearfix">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="panel widget light-widget">
                                            <div className="panel-body table-responsive">
                                                <div className="btn vd_bg_cust white-link pull-right" onClick={()=>{this.setState({show:true})}}>
                                                    <Translate id="addPrice"/>
                                                    <i className="icon-add-to-list white-link ml-2"/>

                                                </div>
                                                {this.state.show_skeleton === true?
                                                    <Skeleton count={10}/>:
                                                    <DataTable
                                                        title="Price"
                                                        className="rtl"
                                                        columns={columns}
                                                        data={data}
                                                        highlightOnHover
                                                        pagination
                                                        pointerOnHover
                                                        defaultSortField="name"
                                                        noHeader
                                                    />
                                                }
                                            </div>

                                            <Modals  title={this.state.modalTitle} show={this.state.show} handleShow={this.hidemodal.bind(this)}>
                                                <FormMaker
                                                    url={this.state.url_form}
                                                    getResponse={this.getResponse.bind(this)}
                                                    value={this.state.editedItem}
                                                    contain_image={true}
                                                    inputs={
                                                        [
                                                            {name:"product_id",
                                                                type:'select',
                                                                is_multi:false,
                                                                opt:this.state.pro_opt,
                                                                label:<Translate id={"productSelect"}/>},
                                                            {name:"vendor_id",
                                                                type:'select',
                                                                is_multi:false,
                                                                opt:this.state.ven_opt,
                                                                label:<Translate id={"vendorSelect"}/>},
                                                            {name:"single_price",type:"number",label:<Translate id={"singlePrice"}/>},
                                                            {name:"bulk_price",type:"number",label:<Translate id={"bulkPrice"}/>},
                                                            {name:"minimum_bulk",type:"number",label:<Translate id={"minBulk"}/>}
                                                        ]}
                                                    edit_id={this.state.edit_id}
                                                    method={this.state.method_form}
                                                />
                                            </Modals>
                                            <Modals title={ <Translate id="delete"/>}  show={this.state.delete_show} handleShow={(show)=>{this.setState({delete_show:show,cat_delete:0})}}>
                                                <div className="d-flex justify-content-end rtl">
                                                    <Translate id="welcome"/>

                                                </div>
                                                <div className="d-flex justify-content-center mt-4">
                                                    <div className="btn btn-danger" onClick={this.DeletePrice.bind(this)}>
                                                        <Translate id="delete"/>
                                                    </div>
                                                </div>
                                            </Modals>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default Price
