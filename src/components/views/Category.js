import React from 'react';
import Button from "react-bootstrap/es/Button";
import DataTable from 'react-data-table-component';
import Modals from "../Sections/Modals";
import conf from "../../config";
import FormMaker from "../Sections/FormMaker";
import Skeleton from 'react-loading-skeleton';
import Expanded from '../Sections/Expanded';
import * as axios from 'axios';
import ButtonGroup from "react-bootstrap/es/ButtonGroup";
import {maker_opt} from "../helper";
import {Translate} from "react-localize-redux";


class Category extends React.Component {
   constructor(){
       super();
       this.state={
           show:false,
           cat_opt:[],
           attr_opt:[],
           isHoveredIcon:false,
           items: [],
           editedItem:{},
           show_skeleton:true,
           modalTitle:<Translate id="addCategory"/>,
           url_form:conf.server+"/category",
           method_form:"POST",
           cat_delete:0,
           delete_show:false


       };
       this.toggleHover=this.toggleHover.bind(this);
       this.loadParentAndCats=this.loadParentAndCats.bind(this)
       this.loadAttributes=this.loadAttributes.bind(this);
       this.handleBtnEdit=this.handleBtnEdit.bind(this)
       this.setDeletedId=this.setDeletedId.bind(this)
       this.DeleteCategory=this.DeleteCategory.bind(this)

   }
    componentDidMount(){
        this.loadParentAndCats();
        this.loadAttributes();
    }



    hidemodal(show,url=conf.server+"/category",method="POST"){

        this.setState({
            show:show,
            url_form:url,
            method_form:method,
            modalTitle:<Translate id="addCategory"/>
        })
    }

    toggleHover(){
        this.setState(prevState => ({isHoveredIcon: !prevState.isHoveredIcon}));
    }

    loadParentAndCats(){
        var settings = {
            "url": conf.server+"/category",
            "method": "GET",
            "timeout": 0,
        };
        let that=this;
      axios(settings).then(function (response) {
         let options= maker_opt(response.data.data,"name","id");

         that.setState({cat_opt:options,items:response.data.data,show_skeleton:false})
        });
    }

    loadAttributes(){
        var settings = {
            "url": conf.server+"/attribute",
            "method": "GET",
            "headers": {
                "Accept": "application/json"
            },
            "timeout": 0,
        };
        axios(settings).then( (response)=> {
            let options= maker_opt(response.data.data,"name","id");
            this.setState({attr_opt:options});
        });

    }


    getResponse(){

            this.setState({
                show:false,
                modalTitle:<Translate id={"addCategory"}/>
            });
            this.loadParentAndCats()
   }

    handleBtnEdit(id){
        console.log('Selected edit Row: ', id);
        this.setState({
            show:true,
            modalTitle:<Translate id="editCategory"/>

        });
        axios({
            method: 'GET',
            url: conf.server+"/category/"+id,

        }).then(res=>{
            console.log('resItem',res.data);
            let cat=res.data.data;
            let editedItem={};
            editedItem['attribute_ids']=cat['attributes'].map(obj=>obj["id"]);
            editedItem['name']=cat['name'];

            this.setState({
                editedItem,
                url_form:conf.server+"/category/"+id,
                method_form:"PUT"
            })

        })
    }


    setDeletedId(id){
       this.setState({delete_show:true,cat_delete:id});
    }

    DeleteCategory(){
        axios({
            "url": conf.server+"/category/"+this.state.cat_delete,
            "method": "DELETE",
            "timeout": 0,
        }).then( (response) =>{
            this.setState({cat_delete:0, delete_show:false});
            this.loadParentAndCats()
        });
    }

    render() {
       const columns = [
           {
               name:'id',
               selector:'id',
               omit:true
           },
           {

               name:  <Translate id="categoryName"/>,
               selector: 'name',
               sortable: true,
               style: {
                   color: '#202124',
                   fontSize: '14px',
                   fontWeight: 500,
               }
           },
           {

               cell: (row) => {

                   return  <div className="menu-action" >
                       <div
                           onClick={()=>this.handleBtnEdit(row.id)}
                           className="btn menu-icon vd_yellow"> <i
                           className="fa fa-pencil"/> </div>
                       <div
                           onClick={()=>this.setDeletedId(row.id)}
                           className="btn menu-icon vd_red"> <i
                           className="fa fa-times"/> </div>

                   </div>


               },
               allowOverflow: true,
               button: true,
               width: '200px'
           }
       ];


        let  data =[];
        this.state.items.map(obj=> obj['parent_id']===null?data.push(obj):"");
        console.log(data,'here');
        let form_inputs;
        if(this.state.method_form!=='PUT'){
            form_inputs=[{name:"parent_id",
                type:'select',
                is_multi:false,
                opt:this.state.cat_opt,
                label:<Translate id={"category"}/>},
                {name:"attribute_ids",
                    type:'select',
                    is_multi:true,
                    opt: this.state.attr_opt,
                    label:<Translate id={"attributeSelect"}/>,
                    value_in_receive:"attributes"},
                {name:"name",type:"text",label:<Translate id={"name"}/>,}];
        }else{
            form_inputs=[
                {name:"attribute_ids",
                    type:'select',
                    is_multi:true,
                    opt: this.state.attr_opt,
                    label:<Translate id={"category"}/>,
                    value_in_receive:"attributes"},
                {name:"name",
                    label:<Translate id={"name"}/>,
                 type:"text"}];
        }



       return (
           <div className="vd_content-wrapper">
               <div className="vd_container">
                   <div className="vd_content clearfix">
                       <div className="vd_title-section clearfix">
                           <div className="vd_content-section clearfix">
                               <div className="row">
                                   <div className="col-md-12">
                                       <div className="panel widget light-widget">

                                           <div className="panel-body table-responsive">
                                               <div className="btn vd_bg_cust white-link pull-right" onClick={()=>{this.hidemodal(true)}}>
                                                   <Translate id="addCategory"/>
                                                   <i className="icon-add-to-list white-link ml-2"/>

                                               </div>
                                               {this.state.show_skeleton === true?
                                                   <Skeleton count={10}/>:
                                                   <DataTable
                                                       className={"rtl bg-grey"}
                                                       style={{"background-opacity":"0.1"}}
                                                       columns={columns}
                                                       data={data}
                                                       className="rtl"
                                                       highlightOnHover
                                                       pagination
                                                       pointerOnHover
                                                       subHeader={false}
                                                       expandableIcon={{collapsed:<span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                           , expanded:  <span className="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>}}
                                                       expandableRows
                                                       expandableRowDisabled={data=> data.children.length===0}
                                                       noHeader
                                                       defaultSortField="category"
                                                       expandableRowsComponent={<Expanded handleBtnEdit={this.handleBtnEdit} handleBtnDelete={this.setDeletedId}/>}

                                                   />
                                               }
                                           </div>

                                           <Modals title={this.state.modalTitle} show={this.state.show} handleShow={this.hidemodal.bind(this)}>
                                               <FormMaker
                                                   edit_id={this.state.edit_id}
                                                   url={this.state.url_form}
                                                   contain_image={false}
                                                   method={this.state.method_form}
                                                   getResponse={this.getResponse.bind(this)}
                                                   value={this.state.editedItem}
                                                   inputs={form_inputs}
                                                    />
                                           </Modals>

                                           <Modals title={ <Translate id="delete"/>}  show={this.state.delete_show} handleShow={(show)=>{this.setState({delete_show:show,cat_delete:0})}}>
                                               <div className="d-block text-center mb-2">
                                                   <Translate id="welcome"/>
                                                   <div className="d-block text-center mt-2">
                                                       <div className="btn btn-danger" onClick={this.DeleteCategory.bind(this)}>
                                                           <Translate id="delete"/>
                                                       </div>
                                                   </div>
                                               </div>
                                           </Modals>

                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

       )
   }
}

export default Category
