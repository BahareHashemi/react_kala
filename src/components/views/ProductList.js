import React from 'react';
import Button from "react-bootstrap/es/Button";
import DataTable from 'react-data-table-component';
import Modals from "../Sections/Modals";
import conf from "../../config";
import FormMaker from "../Sections/FormMaker";
import Skeleton from 'react-loading-skeleton';
import * as axios from 'axios';
import ButtonGroup from "react-bootstrap/es/ButtonGroup";
import {maker_opt} from "../helper";
import {Translate} from "react-localize-redux";
import Link from "react-router-dom/es/Link";


class ProductList extends React.Component {
    constructor(){
        super();
        this.state={
            show:false,
            isHoveredIcon:false,
            show_skeleton:true,
            items: [],
            editedItem:{},
            url_form:conf.server+"/product",
            edit_id:0,
            method_form:"POST",
            pro_delete:0,
            delete_show:false
        };

        this.loadProducts=this.loadProducts.bind(this);
        this.handleBtnEdit=this.handleBtnEdit.bind(this);
        this.setDeletedId=this.setDeletedId.bind(this);
        this.DeleteProduct=this.DeleteProduct.bind(this);

    }
    componentDidMount(){

        this.loadProducts();
    }


    loadProducts(){

        var settings = {
            "url": conf.server+"/product",
            "method": "GET",
            "timeout": 0,
        };
        let that=this;
        axios(settings).then(function (response) {
            console.log('res',response.data.data);
            let res=response.data.data;
            let tmp={};
            let data=[];
            Object.values(res).map((r)=>{
                tmp['id']=r['id'];
                tmp['name']=r['name'];
                tmp['category_id']=r['category']['name'];
                tmp['brand_id']=r['brand']['name'];
                tmp['pic']=r['pic'];
                data.push(tmp)
                tmp={}
            });
            console.log("data",data)


            that.setState({items:data,show_skeleton:false})
        });
    }

    getResponse(){

        this.setState({show:false});
        this.loadProducts()
    }

    handleBtnEdit(id){
        console.log('Selected edit Row: ', id);

    }

    setDeletedId(id){
        this.setState({delete_show:true,pro_delete:id});
    }

    DeleteProduct(){
        axios({
            "url": conf.server+"/product/"+this.state.pro_delete,
            "method": "DELETE",
            "timeout": 0,
        }).then( (response) =>{
            this.setState({pro_delete:0, delete_show:false});
            this.loadProducts();
        });
    }

    render() {

        const data=this.state.items;
        const columns = [
            {
                name:'id',
                selector:'id',
                omit:true
            },
            {

                name:  <Translate id="picTable"/>,
                selector: 'picture',
                cell: (row)=> <img className="p-1" style={{borderRadius:"1rem"}} width="84px" height="56px" src={row.pic}/>


            },
            {

                name:  <Translate id="productName"/>,
                selector: 'name',
                sortable: true,
                style: {
                    color: '#202124',
                    fontSize: '14px',
                    fontWeight: 500,
                }
            },
            {
                name:'pic' ,
                selector:'pic',
                omit:true

            },
            {

                name:  <Translate id="category"/>,
                selector: 'category_id',
                sortable: true,

            },
            {

                name:  <Translate id="brand"/>,
                selector: 'brand_id',
                sortable: true,
                style: {
                    color: '#202124',
                    fontSize: '14px',
                    fontWeight: 500,
                }
            },
            {

                cell: (row) => {

                    return  <div className="menu-action" >
                        {/*<div*/}
                        {/*    className="btn menu-icon">*/}
                        {/*    <Link push to="/addProduct" className="vd_yellow" ><i*/}
                        {/*    className="fa fa-pencil"/>   </Link>*/}
                        {/*</div>*/}

                        <div
                            onClick={()=>this.setDeletedId(row.id)}
                            className="btn menu-icon vd_red"> <i
                            className="fa fa-times"/> </div>

                    </div>



                },
                allowOverflow: true,
                button: true,
                width: '200px'
            }
        ];

        return (
            <div className="vd_content-wrapper">
                <div className="vd_container">
                    <div className="vd_content clearfix">
                        <div className="vd_title-section clearfix">
                            <div className="vd_content-section clearfix">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="panel widget light-widget">

                                            <div className="panel-body table-responsive">
                                                <Link push to="/addProduct" className="btn  vd_bg_cust white-link pull-right" >
                                                    <Translate id="gotoAddProduct"/>
                                                    <Link push to="/addProduct" >
                                                        <i className="icon-add-to-list white-link ml-2"/>
                                                    </Link>

                                                </Link>
                                                {this.state.show_skeleton === true?
                                                    <Skeleton count={10}/>:
                                                    <DataTable
                                                        title="Product"
                                                        columns={columns}
                                                        data={data}
                                                        className="rtl"
                                                        highlightOnHover
                                                        pagination
                                                        pointerOnHover
                                                        defaultSortField="name"
                                                        noHeader
                                                    />
                                                }
                                            </div>

                                            <Modals title={ <Translate id="delete"/>}  show={this.state.delete_show} handleShow={(show)=>{this.setState({delete_show:show,pro_delete:0})}}>
                                                <div className="d-flex justify-content-end text-right">
                                                    <Translate id="welcome"/>

                                                </div>
                                                <div className="d-flex justify-content-center mt-4">
                                                    <div className="btn btn-danger" onClick={this.DeleteProduct.bind(this)}>
                                                        <Translate id="delete"/>
                                                    </div>
                                                </div>
                                            </Modals>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default ProductList

