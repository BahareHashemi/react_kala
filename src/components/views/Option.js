import React from 'react';
import Button from "react-bootstrap/es/Button";
import DataTable from 'react-data-table-component';
import Modals from "../Sections/Modals";
import conf from "../../config";
import FormMaker from "../Sections/FormMaker";
import Skeleton from 'react-loading-skeleton';
import * as axios from 'axios';
import ButtonGroup from "react-bootstrap/es/ButtonGroup";
import {maker_opt} from "../helper";
import {Translate} from "react-localize-redux";
import SingleExpand from "../Sections/SingleExpand";
import {connect} from "react-redux";
import {opt_reload} from "../../action/options_action";


class Option extends React.Component {

    constructor(){
        super();
        this.state={
            show:false,
            isHoveredIcon:false,
            show_skeleton:true,
            items: [],
            editedItem:{},
            modalTitle:'Add Option',
            url_form:conf.server+"/option",
            method_form:"POST",
            opt_delete:0,
            delete_show:false

        };

        this.loadAttributes=this.loadAttributes.bind(this);
        this.DeleteOption=this.DeleteOption.bind(this);

    }
    componentDidMount(){

        this.loadAttributes();

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.reload_option!==this.props.reload_option && this.props.reload_option){
           this.props.opt_reload(false);
           this.loadAttributes();
        }
    }

    hidemodal(show,url=conf.server+"/option",method="POST"){

        this.setState({
            show:show,
            url_form:url,
            method_form:method
        })
    }


    loadAttributes(){

        var settings = {
            "url": conf.server+"/attribute",
            "method": "GET",
            "timeout": 0,
        };
        let that=this;
        axios(settings).then(function (response) {
            let options= maker_opt(response.data.data,"name","id");
            that.setState({items:response.data.data,show_skeleton:false,att_opt:options})
        });
    }


    getResponse(){

        this.setState({show:false});
        this.loadAttributes()
    }


    DeleteOption(id){
        axios({
            "url": conf.server+"/option/"+id,
            "method": "DELETE",
            "timeout": 0,
        }).then( (response) =>{
            this.loadAttributes();

        });
    }

    showBtnOption(id){
        this.setState({
            show:true,
            modalTitle:'Add Option',
            att_id:id
        })
    }


    render() {

        const data=this.state.items;
        const columns = [
            {
                name:'id',
                selector:'id',
                omit:true
            },
            {

            name:  <Translate id="attributeName"/>,
            selector: 'name',
            sortable: true,
            style: {
                color: '#202124',
                fontSize: '14px',
                fontWeight: 500,
            }
            },
            {

            cell: (row) => {

                return  <ButtonGroup size="sm">
                    <Button onClick={()=>this.showBtnOption(row.id)} className="btn btn-primary btn-sm">
                        <Translate id="addOption"/>
                    </Button>


                </ButtonGroup>


                },
                allowOverflow: true,
                button: true,
                width: '200px'
            }
        ];

        return (
            <div className="vd_content-wrapper">
                <div className="vd_container">
                    <div className="vd_content clearfix">
                        <div className="vd_title-section clearfix">
                            <div className="vd_content-section clearfix">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="panel widget light-widget">

                                            <div className="panel-body table-responsive">
                                                {this.state.show_skeleton === true?
                                                    <Skeleton count={10}/>:
                                                    <DataTable
                                                        title="Attribute"
                                                        className="rtl"
                                                        columns={columns}
                                                        data={data}
                                                        highlightOnHover
                                                        pagination
                                                        pointerOnHover
                                                        subHeader={false}
                                                        expandableRows
                                                        expandableRowExpanded={(data)=>{
                                                            return data.options.length>0 ;
                                                        }}
                                                        noHeader
                                                        defaultSortField="attribute"
                                                        expandableRowsComponent={ <SingleExpand  handleBtnDelete={this.DeleteOption}/>}
                                                    />
                                                }
                                            </div>

                                            <Modals  title={this.state.modalTitle} show={this.state.show} handleShow={this.hidemodal.bind(this)}>
                                                <FormMaker
                                                    url={this.state.url_form}
                                                    getResponse={this.getResponse.bind(this)}
                                                    value={this.state.editedItem}
                                                    contain_image={false}
                                                    inputs={
                                                        [
                                                            {name:"attribute_id",
                                                                type:'select',
                                                                is_multi:false,
                                                                opt:this.state.att_opt,
                                                                lable:"Attribute"},
                                                            {name:"names",type:"tagsinput",lable:"Option"}

                                                        ]}

                                                    method={this.state.method_form}
                                                />
                                            </Modals>
                                            <Modals title={ <Translate id="delete"/>}  show={this.state.delete_show} handleShow={(show)=>{this.setState({delete_show:show,opt_delete:0})}}>
                                                <div className="d-flex text-right justify-content-end ">
                                                    <Translate id="welcome"/>

                                                </div>
                                                <div className="d-flex justify-content-center mt-4">
                                                    <div className="btn btn-danger" onClick={this.DeleteOption.bind(this)}>
                                                        <Translate id="delete"/>
                                                    </div>
                                                </div>
                                            </Modals>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
function mapStateToProps(state) {

    return { reload_option: state.reload_option }
}
export default connect(mapStateToProps,{opt_reload})(Option)
