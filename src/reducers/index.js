import {combineReducers} from 'redux'
import {createReducer} from "@reduxjs/toolkit";
import {logout, reload_opts, set_user} from '../config'


const reload_option=createReducer(false,{
    [reload_opts]:(state, action) =>action.payload,
});


const  user=createReducer({},{
    [set_user]:(state,action)=> {
        console.log(action,'after login')
        return action.payload
    },
    [logout]:(state,action)=>({})
})


export default combineReducers({reload_option,user});
