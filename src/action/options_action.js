import {createAction} from "@reduxjs/toolkit";
import {reload_opts} from "../config";
export const opt_reload=createAction(reload_opts);
