import {createAction} from "@reduxjs/toolkit";
import {set_user,logout} from "../config";
export const user_setter=createAction(set_user);
export const logout_user=()=>{
    localStorage.setItem('token',null);
    return (dispatch)=>{
        dispatch({type:logout,payload: {}})
    }
};
